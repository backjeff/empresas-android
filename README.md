![N|Solid](logo_ioasys.png)

# EMPRESAS ANDROID #

Aplicativo desenvolvido como teste técnico para Ioasys. Instruções do escopo em `INSTRUCOES.md`.

Confiram os commits para verificar a execução do projeto

### O PROJETO ###

* Desenvolvido em [Kotlin](https://kotlinlang.org/)

* Login e acesso de Usuário já registrado
* Listagem de Empresas
* Detalhamento de Empresas

### BIBLIOTECAS UTILIZADAS ###
    
* [com.google.android.material:material:{version}](https://material.io/develop/android/docs/getting-started/) Componentes de Layout do Material Design do Google
* [com.google.code.gson:gson:{version}](https://github.com/google/gson) Converter Objetos em JSON e vice-versa
* [com.squareup.retrofit2:retrofit:{version}](https://square.github.io/retrofit/) Cliente HTTP usado nos Services
* [androidx.lifecycle:lifecycle-extensions:{version}](https://developer.android.com/jetpack/androidx/releases/lifecycle) ViewModels e LiveData
* [junit:junit:{version}](https://junit.org/junit5/) Testes unitários em EmailValidatorTest
* [org.jetbrains.anko:anko:{version}](https://github.com/Kotlin/anko) Utilizado para gerar toast()
* [com.github.bumptech.glide:glide::{version}](https://github.com/bumptech/glide) Image loader and caching

**Importante:** As imagens não estão carregando pois o endereço `https://empresas.ioasys.com.br/{IMAGE_ADDRESS_FROM_API}` não está acessível. Enviei um email com a dúvida para `vagas@ioasys.com.br`

Ex.: `https://empresas.ioasys.com.br/uploads/enterprise/photo/34/americamg.png`
    
### O QUE EU FARIA COM MAIS TEMPO PARA DESENVOLVIMENTO ###
    
* Mais testes unitários
* Injeção de dependências
* Toasts e mensagens de erros de acordo com API
* Verificação de access_token expirado
* SplashScreen

    Obs: Normalmente utilizo EventBus, mas neste projeto simples talvez não se aplicaria

### Executando a aplicação ###

No terminal execute
    
`git clone https://bitbucket.org/backjeff/empresas-android.git`

Há um instalador na raiz: `empresas-android.apk` para facilitar o teste de interface

Recomendável o Android Studio para executar a aplicação

Em caso de dúvidas entre em contato pelo email `jefferson.rodrigues.mail@gmail.com`

### Dados para Teste ###

* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

### Bônus concluídos ###

* :check: Testes unitários em EmailValidatorTest
* :check: MVVM
* :check: Material Design
* :check: LiveData, Coroutines
* :check: Design patterns: Builder, Adapter, Facade, MVVM
