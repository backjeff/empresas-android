package br.com.ioasys.empresasandroid.ui.enterprise

import androidx.lifecycle.LiveData
import br.com.ioasys.empresasandroid.data.remote.response.EnterprisesResponse

interface EnterpriseInterface {
    fun onStarted()
    fun onSuccess(enterprisesResponse: LiveData<EnterprisesResponse>)
    fun onError(message: String?)
}