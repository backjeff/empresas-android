package br.com.ioasys.empresasandroid.ui.home

import br.com.ioasys.empresasandroid.domain.model.Enterprise

interface HomeInterface {
    fun logOut()
    fun itemClick(enterprise: Enterprise)
}