package br.com.ioasys.empresasandroid.di.modules

import br.com.ioasys.empresasandroid.ui.login.LoginViewModel
import org.koin.dsl.module

val viewModelModules = module {
    single { LoginViewModel() }
}