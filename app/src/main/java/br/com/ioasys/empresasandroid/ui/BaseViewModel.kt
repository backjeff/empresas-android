package br.com.ioasys.empresasandroid.ui

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel

open class BaseViewModel: ViewModel() {
    val isLoading = ObservableBoolean(false)
}