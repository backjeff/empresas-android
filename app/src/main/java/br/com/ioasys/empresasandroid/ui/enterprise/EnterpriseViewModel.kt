package br.com.ioasys.empresasandroid.ui.enterprise

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.ioasys.empresasandroid.data.remote.RetrofitBuilder
import br.com.ioasys.empresasandroid.data.remote.response.EnterprisesResponse
import br.com.ioasys.empresasandroid.data.remote.response.HeaderResponse
import br.com.ioasys.empresasandroid.data.remote.service.EnterprisesService
import br.com.ioasys.empresasandroid.ui.BaseViewModel
import retrofit2.Call
import retrofit2.Callback

class EnterpriseViewModel: BaseViewModel() {

    var enterpriseInterface: EnterpriseInterface? = null

    var enterprisesService: EnterprisesService = RetrofitBuilder().newInstance().create(EnterprisesService::class.java)

    fun show(headerResponse: HeaderResponse, id: Int?): LiveData<EnterprisesResponse> {
        enterpriseInterface?.onStarted()

        val enterprisesResponse = MutableLiveData<EnterprisesResponse>()

        enterprisesService.show(
            headerResponse.access_token,
            headerResponse.client,
            headerResponse.uid,
            id.toString()
        )
            .enqueue(object: Callback<EnterprisesResponse> {
                override fun onFailure(call: Call<EnterprisesResponse>, t: Throwable) {
                    enterprisesResponse.value?.success = false
                    enterpriseInterface?.onError(t.message)
                }

                override fun onResponse(call: Call<EnterprisesResponse>, response: retrofit2.Response<EnterprisesResponse>) {
                    if(response.isSuccessful) {
                        enterprisesResponse.value = response.body()
                        enterpriseInterface?.onSuccess(enterprisesResponse)
                    } else {
                        enterprisesResponse.value?.success = false
                        enterpriseInterface?.onError(response.message())
                    }

                }

            })

        return enterprisesResponse
    }

}