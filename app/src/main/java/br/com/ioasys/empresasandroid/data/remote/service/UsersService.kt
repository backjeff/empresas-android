package br.com.ioasys.empresasandroid.data.remote.service

import br.com.ioasys.empresasandroid.app.Constants
import br.com.ioasys.empresasandroid.domain.model.Auth
import br.com.ioasys.empresasandroid.data.remote.response.SignInResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface UsersService {

    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    @POST(Constants.Services.SIGN_IN)
    suspend fun signIn(@Body auth: Auth): Response<SignInResponse>

}