package br.com.ioasys.empresasandroid.data.remote.service

import br.com.ioasys.empresasandroid.app.Constants
import br.com.ioasys.empresasandroid.data.remote.response.EnterprisesResponse
import retrofit2.Call
import retrofit2.http.*

interface EnterprisesService {

    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    @GET("${Constants.Services.ENTERPRISES}/")
    fun index(
        @Header("access-token") accessToken: String?,
        @Header("client") client: String?,
        @Header("uid") uid: String?,
        /*@Query("enterprise_types") enterpriseTypes: String?,*/
        @Query("name") name: String?
    ): Call<EnterprisesResponse>



    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    @GET("${Constants.Services.ENTERPRISES}/{id}")
    fun show(
        @Header("access-token") accessToken: String?,
        @Header("client") client: String?,
        @Header("uid") uid: String?,
        @Path("id") id: String?
    ): Call<EnterprisesResponse>

}