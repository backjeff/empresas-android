package br.com.ioasys.empresasandroid.domain.model

data class Auth (
    val email: String?,
    val password: String?
)