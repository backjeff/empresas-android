package br.com.ioasys.empresasandroid.data.remote.response

import br.com.ioasys.empresasandroid.domain.model.Enterprise
import br.com.ioasys.empresasandroid.domain.model.Investor
import okhttp3.Headers

class SignInResponse (
    var success: Boolean? = true,
    var investor: Investor? = null,
    var enterprise: Enterprise? = null,
    var headers: Headers? = null
)