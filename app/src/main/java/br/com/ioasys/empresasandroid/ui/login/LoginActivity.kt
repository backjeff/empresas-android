package br.com.ioasys.empresasandroid.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import br.com.ioasys.empresasandroid.BuildConfig
import br.com.ioasys.empresasandroid.R
import br.com.ioasys.empresasandroid.app.util.FlowState
import br.com.ioasys.empresasandroid.data.local.PreferencesManager
import br.com.ioasys.empresasandroid.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val preferences by lazy { PreferencesManager(this) }
    private val viewModel by lazy { LoginViewModel() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
        mockDebug()
        subscribeViewModel()

    }

    private fun init() {
        viewModel.isLoading.set(false)
        if(preferences.getString("access_token").isNotEmpty()) {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }

    }

    private fun mockDebug() {
        if (BuildConfig.DEBUG) {
            email.setText("testeapple@ioasys.com.br")
            password.setText("12341234")
        }
    }

    private fun subscribeViewModel() {
        viewModel.getLoginStatus().observe(this, Observer {
            when(it.status) {
                FlowState.Status.LOADING -> { viewModel.isLoading.set(true) }
                FlowState.Status.SUCCESS -> handleSuccess()
                FlowState.Status.ERROR -> { viewModel.isLoading.set(false) }
            }
        })
    }

    private fun handleSuccess() {
        viewModel.isLoading.set(false)

        viewModel.mHeaders?.apply {
            preferences.setValue("access_token", get("access-token")?: "" )
            preferences.setValue("client", get("client")?: "" )
            preferences.setValue("uid", get("uid-token")?: "" )
        }

        startActivity(Intent(this, HomeActivity::class.java))
        finish()

    }

}
