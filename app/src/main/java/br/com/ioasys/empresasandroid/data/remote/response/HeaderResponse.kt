package br.com.ioasys.empresasandroid.data.remote.response

class HeaderResponse (
    var access_token: String? = null,
    var client: String? = null,
    var uid: String? = null
)