package br.com.ioasys.empresasandroid.ui.login

import androidx.lifecycle.LiveData
import br.com.ioasys.empresasandroid.data.remote.response.SignInResponse

interface LoginInterface {
    fun onStarted()
    fun onSuccess(signInResponse: SignInResponse)
    fun onError(message: String)
}