package br.com.ioasys.empresasandroid.ui.enterprise

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.ioasys.empresasandroid.R
import br.com.ioasys.empresasandroid.app.Constants.URL
import br.com.ioasys.empresasandroid.data.local.PreferencesManager
import br.com.ioasys.empresasandroid.data.remote.response.EnterprisesResponse
import br.com.ioasys.empresasandroid.data.remote.response.HeaderResponse
import br.com.ioasys.empresasandroid.databinding.ActivityEnterpriseBinding
import br.com.ioasys.empresasandroid.domain.model.Enterprise
import com.bumptech.glide.Glide
import org.jetbrains.anko.toast

class EnterpriseActivity : AppCompatActivity(), EnterpriseInterface {

    private lateinit var binding: ActivityEnterpriseBinding
    private val preferences by lazy { PreferencesManager(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_enterprise)

        setSupportActionBar(binding.toolbar)

        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        binding.enterprise = intent.extras?.get("enterprise") as Enterprise

        if(binding.enterprise == null){
            toast("Empresa não encontrada")
            finish()
        }

        supportActionBar?.title = binding.enterprise?.enterprise_name

        val viewModel = ViewModelProviders.of(this).get(EnterpriseViewModel::class.java)
        binding.viewModel = viewModel
        binding.viewModel?.enterpriseInterface = this
        binding.viewModel?.show(HeaderResponse(
            preferences.getString("access_token"),
            preferences.getString("client"),
            preferences.getString("uid")
        ), binding.enterprise?.id)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onStarted() {
        binding.viewModel?.isLoading?.set(true)
    }

    override fun onSuccess(enterprisesResponse: LiveData<EnterprisesResponse>) {
        enterprisesResponse.observe(this, Observer {
            binding.enterprise = it.enterprise
            binding.viewModel?.isLoading?.set(false)

            Glide.with(this)
                .load(URL + binding.enterprise?.phone)
                .placeholder(R.drawable.ic_crop_original_black_24dp)
                .error(R.drawable.ic_crop_original_error_24dp)
                .into(binding.imageView)
        })
    }

    override fun onError(message: String?) {
        binding.viewModel?.isLoading?.set(false)
    }

}
