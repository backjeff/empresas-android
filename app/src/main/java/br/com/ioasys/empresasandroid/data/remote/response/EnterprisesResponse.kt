package br.com.ioasys.empresasandroid.data.remote.response

import br.com.ioasys.empresasandroid.domain.model.Enterprise

class EnterprisesResponse (
    var enterprises: List<Enterprise>?,
    var enterprise: Enterprise?,
    var success: Boolean? = true
)