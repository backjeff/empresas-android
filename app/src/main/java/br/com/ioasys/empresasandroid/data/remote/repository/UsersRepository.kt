package br.com.ioasys.empresasandroid.data.remote.repository

import br.com.ioasys.empresasandroid.data.remote.RetrofitBuilder
import br.com.ioasys.empresasandroid.domain.model.Auth
import br.com.ioasys.empresasandroid.data.remote.service.UsersService
import br.com.ioasys.empresasandroid.data.remote.response.SignInResponse
import retrofit2.Response

class UsersRepository {

    suspend fun signIn(auth: Auth): Response<SignInResponse> {
        return RetrofitBuilder().newInstance().create(UsersService::class.java).signIn(auth)
    }

}