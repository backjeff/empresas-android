package br.com.ioasys.empresasandroid.ui.login

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import br.com.ioasys.empresasandroid.app.util.Coroutines
import br.com.ioasys.empresasandroid.app.util.FlowState
import br.com.ioasys.empresasandroid.data.remote.repository.UsersRepository
import br.com.ioasys.empresasandroid.data.remote.response.SignInResponse
import br.com.ioasys.empresasandroid.domain.model.Auth
import br.com.ioasys.empresasandroid.domain.model.Enterprise
import br.com.ioasys.empresasandroid.domain.model.Investor
import br.com.ioasys.empresasandroid.ui.BaseViewModel
import okhttp3.Headers

class LoginViewModel : BaseViewModel() {

    var mEnterprise: Enterprise? = null
    var mInvestor: Investor? = null
    var mHeaders: Headers? = null

    val loginStatus by lazy { MediatorLiveData<FlowState<Unit>>() }
    fun getLoginStatus(): LiveData<FlowState<Unit>> = loginStatus

    val email = ObservableField<String>()
    val password = ObservableField<String>()

    var loginInterface: LoginInterface? = null

    fun onEmailChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        email.set(s.toString())
    }

    fun onPasswordChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        password.set(s.toString())
    }

    fun buttonClicked() {
        loginInterface?.onStarted()
        signIn()
    }

    fun signIn() {
        Coroutines.main {
            val signInResponse = UsersRepository().signIn(Auth(email.get(), password.get()))
            if(signInResponse.isSuccessful) {
                signInResponse.body()?.apply {
                    mEnterprise = enterprise
                    mInvestor = investor
                    mHeaders = headers
                    loginStatus.value = FlowState(FlowState.Status.SUCCESS)
                }
            } else {
                loginStatus.value = FlowState(FlowState.Status.ERROR)
            }
        }

    }

}