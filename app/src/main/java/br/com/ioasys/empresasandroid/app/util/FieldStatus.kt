package br.com.ioasys.empresasandroid.app.util

enum class FieldStatus {
    EMPTY, VALID, INVALID
}