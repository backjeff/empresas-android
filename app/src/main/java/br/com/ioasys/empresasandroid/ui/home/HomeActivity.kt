package br.com.ioasys.empresasandroid.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.ioasys.empresasandroid.R
import br.com.ioasys.empresasandroid.app.Constants.LOG_TAG
import br.com.ioasys.empresasandroid.data.local.PreferencesManager
import br.com.ioasys.empresasandroid.data.remote.response.EnterprisesResponse
import br.com.ioasys.empresasandroid.data.remote.response.HeaderResponse
import br.com.ioasys.empresasandroid.databinding.ActivityHomeBinding
import br.com.ioasys.empresasandroid.domain.adapter.EnterprisesAdapter
import br.com.ioasys.empresasandroid.domain.model.Enterprise
import br.com.ioasys.empresasandroid.ui.enterprise.EnterpriseActivity
import br.com.ioasys.empresasandroid.ui.enterprise.EnterpriseInterface
import br.com.ioasys.empresasandroid.ui.login.LoginActivity
import kotlinx.android.synthetic.main.content_home.view.*
import org.jetbrains.anko.toast

class HomeActivity : AppCompatActivity(), EnterpriseInterface, HomeInterface {

    private lateinit var binding: ActivityHomeBinding
    private val preferences by lazy { PreferencesManager(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = null

        val viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.enterpriseInterface = this
        viewModel.homeInterface = this

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)

        val searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                binding.viewModel?.index(
                    HeaderResponse(
                        preferences.getString("access_token"),
                        preferences.getString("client"),
                        preferences.getString("uid")
                    ), query!!
                )

                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return true
            }
        })

        return true
    }

    override fun onStarted() {
        toast("Carregando empresas")
        binding.viewModel?.isLoading?.set(true)
    }

    override fun onSuccess(enterprisesResponse: LiveData<EnterprisesResponse>) {
        enterprisesResponse.observe(this, Observer {
            binding.content.recyclerView.apply {
                layoutManager = LinearLayoutManager(this@HomeActivity)
                adapter = EnterprisesAdapter(it.enterprises!!, this@HomeActivity, this@HomeActivity)
            }

        })
    }

    override fun onError(message: String?) {
        binding.viewModel?.isLoading?.set(false)
        toast("Sessão expirada")
        Log.i(LOG_TAG, message!!)
    }

    override fun logOut() {
        preferences.setValue("access_token", "")
        preferences.setValue("client", "")
        preferences.setValue("uid", "")
        startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
        finish()
    }

    override fun itemClick(enterprise: Enterprise) {
        val intent = Intent(this, EnterpriseActivity::class.java)
        intent.putExtra("enterprise", enterprise)
        startActivity(intent)
    }


}
