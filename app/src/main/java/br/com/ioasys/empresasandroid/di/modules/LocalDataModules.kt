package br.com.ioasys.empresasandroid.di.modules

import br.com.ioasys.empresasandroid.data.local.PreferencesManager
import org.koin.android.ext.koin.androidApplication

import org.koin.dsl.module

val localDataModules = module {
    single { PreferencesManager(androidApplication()) }
}