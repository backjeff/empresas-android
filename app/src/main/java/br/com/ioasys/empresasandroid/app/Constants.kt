package br.com.ioasys.empresasandroid.app

object Constants {

    const val LOG_TAG = "empresas-android"
    const val SHARED = "br.com.ioasys.empresasandroid.app"
    const val URL = "https://empresas.ioasys.com.br/"
    const val API = "api/"
    const val VERSION = "v1/"
    const val ENDPOINT = URL + API + VERSION

    object Services {
        const val SIGN_IN = "users/auth/sign_in"
        const val ENTERPRISES = "enterprises/"
    }

}