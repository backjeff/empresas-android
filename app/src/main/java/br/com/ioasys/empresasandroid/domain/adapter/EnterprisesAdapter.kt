package br.com.ioasys.empresasandroid.domain.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.ioasys.empresasandroid.R
import br.com.ioasys.empresasandroid.app.Constants
import br.com.ioasys.empresasandroid.domain.model.Enterprise
import br.com.ioasys.empresasandroid.ui.home.HomeInterface
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_enterprise.view.*

class EnterprisesAdapter(private val list: List<Enterprise>, private val itemClickListener: HomeInterface? = null, private val context: Context? = null): RecyclerView.Adapter<EnterprisesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val enterprise: Enterprise = list[position]
        holder.bind(enterprise)

        Glide.with(context!!)
            .load(Constants.URL + list[position].phone)
            .placeholder(R.drawable.ic_crop_original_black_24dp)
            .error(R.drawable.ic_crop_original_error_24dp)
            .into(holder.itemView.imageView)

        holder.itemView.setOnClickListener {
            itemClickListener?.itemClick(list[position])
        }

    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_enterprise, parent, false)) {

        private var name: TextView? = null
        private var description: TextView? = null
        private var country: TextView? = null

        init {
            name = itemView.findViewById(R.id.name)
            description = itemView.findViewById(R.id.description)
            country = itemView.findViewById(R.id.country)
        }

        fun bind(enterprise: Enterprise) {
            name?.text = enterprise.enterprise_name
            description?.text = enterprise.enterprise_type?.enterprise_type_name
            country?.text = enterprise.country
        }

    }

}