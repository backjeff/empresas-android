package br.com.ioasys.empresasandroid.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Enterprise (
    val id: Int,
    val email_enterprise: String?,
    val facebook: String?,
    val twitter: String?,
    val linkedin: String?,
    val phone: String?,
    val own_enterprise: Boolean = false,
    val enterprise_name: String?,
    val photo: String?,
    val description: String?,
    val city: String?,
    val country: String?,
    val value: Double = 0.0,
    val share_price: Double = 0.0,
    val enterprise_type: EnterpriseType?
) : Parcelable