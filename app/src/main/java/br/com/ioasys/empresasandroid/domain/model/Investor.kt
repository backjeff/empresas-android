package br.com.ioasys.empresasandroid.domain.model

data class Investor (
    val id: Int,
    val investor_name: String?,
    val email: String?,
    val city: String?,
    val country: String?,
    val balance: Double = 0.0,
    val photo: String?,
    // portfolio
    val portifolio_value: Double = 0.0,
    val first_access: Boolean = false,
    val super_angel: Boolean = false
)