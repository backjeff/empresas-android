package br.com.ioasys.empresasandroid.app

import android.app.Application
import br.com.ioasys.empresasandroid.di.modules.localDataModules
import br.com.ioasys.empresasandroid.di.modules.viewModelModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class EmpresasApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@EmpresasApplication)
            modules(listOf(
                localDataModules,
                viewModelModules
            ))
        }
    }

}