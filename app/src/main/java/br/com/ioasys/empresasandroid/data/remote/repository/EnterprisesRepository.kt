package br.com.ioasys.empresasandroid.data.remote.repository

import br.com.ioasys.empresasandroid.data.remote.RetrofitBuilder
import br.com.ioasys.empresasandroid.data.remote.response.EnterprisesResponse
import br.com.ioasys.empresasandroid.data.remote.response.HeaderResponse
import br.com.ioasys.empresasandroid.data.remote.service.EnterprisesService
import retrofit2.Call

class EnterprisesRepository {

    suspend fun index(headers: HeaderResponse?, enterpriseTypes: String, name: String): Call<EnterprisesResponse> {
        return RetrofitBuilder().newInstance().create(EnterprisesService::class.java).index(
            headers?.access_token,
            headers?.client,
            headers?.uid,
            /*enterpriseTypes,*/
            name
        )
    }

    suspend fun show(headers: HeaderResponse?, id: Int): Call<EnterprisesResponse> {
        return RetrofitBuilder().newInstance().create(EnterprisesService::class.java).show(
            headers?.access_token,
            headers?.client,
            headers?.uid,
            id.toString()
        )
    }


}