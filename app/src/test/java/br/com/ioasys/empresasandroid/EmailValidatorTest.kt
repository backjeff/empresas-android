package br.com.ioasys.empresasandroid

import org.junit.Test
import br.com.ioasys.empresasandroid.app.util.EmailValidator
import org.junit.Assert.assertTrue
import org.junit.Assert.assertFalse

class EmailValidatorTest {

    @Test
    fun emailValidator_validEmail_returnsTrue() {
        assertTrue(EmailValidator.isValidEmail("name@email.com"))
    }

    @Test
    fun emailValidator_emptyEmail_returnsFalse() {
        assertFalse(EmailValidator.isValidEmail(""))
    }

    @Test
    fun emailValidator_invalidEmail_returnsFalse() {
        assertFalse(EmailValidator.isValidEmail("João da Silva"))
    }

}